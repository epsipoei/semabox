import psutil
import mysql.connector
import socket
import subprocess
import datetime
from env import secret

# Connect to the database
conn = mysql.connector.connect(
    host=secret.db_host,
    port=secret.db_port,
    user=secret.db_user,
    password=secret.db_password,
    database=secret.db_name
)
cursor = conn.cursor()

# Get the hostname
hostname = socket.gethostname()
with open('/semabox/secret', 'r') as fichier:
    # Lire chaque ligne du fichier
    lignes = fichier.readlines()

# Initialiser les variables pour stocker les valeurs
nom_client = ""
adresse_client = ""
siret_client = ""
prestataire = ""
subnet = ""

# Parcourir les lignes du fichier et extraire les informations
for ligne in lignes:
    # Séparer la clé et la valeur en utilisant ":" comme séparateur
    cle, valeur = ligne.strip().split(": ")

    # Vérifier la clé et stocker la valeur dans la variable appropriée
    if cle == "Nom Entreprise":
        nom_client = valeur
    elif cle == "Adresse Entreprise":
        adresse_client = valeur
    elif cle == "Siret Entreprise":
        siret_client = valeur
    elif cle == "Nom Prestataire":
        prestataire = valeur
    elif cle == "subnet":
        subnet = valeur
    elif cle == "num_sonde":
        num_sonde = valeur

# Collect system_info
cpu_info = psutil.cpu_count(logical=False)
ram_info = psutil.virtual_memory().total / (1024 ** 3)  # Convert to gigabytes

# Collect disk_data_value
disk_partitions = psutil.disk_partitions()
disk_info = []
for partition in disk_partitions:
    disk_info.append((partition.device, partition.mountpoint, psutil.disk_usage(partition.mountpoint).total))

# Put data in variables for disk_values
for i, disk in enumerate(disk_info):
    # device = disk[0]
    device = "Nvme"
    mountpoint = disk[1]
    total_size = round(disk[2] / (1024 ** 3), 1)  # Convert to gigabytes with 1 decimal place

# Get SEMAOS version
sema_vers = subprocess.run("cat /semabox/lib/style_menu.py | grep 'version =' | awk '{print $3}'", shell=True, capture_output=True)
version_semaos = sema_vers.stdout.decode().strip()

# Get subnet
ip_addr = subprocess.run("ip a | grep 'eth0' | grep 'inet' | awk '{print $2}' | cut -d '/' -f 1", shell=True, capture_output=True)
ip_default = ip_addr.stdout.decode().strip()

# Get network name for the LAN table
name_network = subprocess.run(f"host {ip_default} | awk '{{print $5}}' | cut -d '.' -f 2", shell=True, capture_output=True)
network_name = name_network.stdout.decode().strip()

# Get VPN IP
sema_ip_vpn = subprocess.run("cat /etc/wireguard/wg-mspr.conf | grep 'Address = ' | awk -F'Address = ' '{print $2}' | sed 's/\/32//g'", shell=True, capture_output=True)
ip_vpn = sema_ip_vpn.stdout.decode().strip()

# Insert data into the database
try:
    #####################################
    # Insert SEMAOS version information #
    #####################################
    date_creation_os = datetime.date.today()
    cursor.execute("INSERT INTO SEMAOS (version_semaos, date_creation_os) VALUES (%s, %s)",
                   (version_semaos, date_creation_os))
    conn.commit()

    # Get the last inserted value
    cursor.execute("SELECT LAST_INSERT_ID()")
    id_semaos = cursor.fetchone()[0]
    
    ################################################
    # Check if num_sonde exists in the SONDE table #
    ################################################
    cursor.execute("SELECT num_sonde FROM SONDE WHERE num_sonde = %s", (num_sonde,))
    result = cursor.fetchone()
    if result:
        # Record already exists, update it
        cursor.execute("UPDATE SONDE SET nom_sonde = %s, cpu_sonde = %s, ram_sonde = %s, id_semaos = %s WHERE num_sonde = %s",
                       (f"semabox-{hostname}", cpu_info, ram_info, id_semaos, num_sonde))
    else:
        # Record does not exist, insert a new one
        cursor.execute("INSERT INTO SONDE (num_sonde, nom_sonde, cpu_sonde, ram_sonde, id_semaos) VALUES (%s, %s, %s, %s, %s)",
                       (num_sonde, f"semabox-{hostname}", cpu_info, ram_info, id_semaos))
    conn.commit()
    
    ######################################################
    # Check if num_sonde exists in the DISQUE_SEMA table #
    ######################################################
    cursor.execute("SELECT num_sonde FROM DISQUE_SEMA WHERE num_sonde = %s", (num_sonde,))
    result = cursor.fetchone()
    if result:
        cursor.execute("UPDATE DISQUE_SEMA SET type_disque = %s, taille_disque = %s WHERE num_sonde = %s",
                       (device, total_size, num_sonde))
    else:
        # Insert disk information
        cursor.execute("INSERT INTO DISQUE_SEMA (num_sonde, type_disque, taille_disque) VALUES (%s, %s, %s)",
                       (num_sonde, device, total_size))
    conn.commit()
    
    ###########################################
    # Check if id_lan exists in the LAN table #
    ###########################################
    cursor.execute("SELECT id_lan FROM LAN WHERE nom_reseau = %s AND adresse_reseau = %s", (network_name, subnet))
    id_lan_result = cursor.fetchone()

    if id_lan_result:
        id_LAN = id_lan_result[0]
    else:
        # If id_lan doesn't exist, you may want to insert it into the LAN table first
        cursor.execute("INSERT INTO LAN (adresse_reseau, nom_reseau) VALUES (%s, %s)",
                       (subnet, network_name))
        conn.commit()

    # Retrieve the id_LAN for the newly inserted record
    cursor.execute("SELECT LAST_INSERT_ID()")
    id_LAN = cursor.fetchone()[0]

    #######################################################
    # Check if num_sonde exists in the RESEAU_SONDE table #
    #######################################################
    cursor.execute("SELECT num_sonde FROM RESEAU_SONDE WHERE num_sonde = %s", (num_sonde,))
    result = cursor.fetchone()

    if result:
        # Update data into RESEAU_SONDE
        cursor.execute("UPDATE RESEAU_SONDE SET id_LAN = %s, ip_defaut_sonde = %s WHERE num_sonde = %s",
                       (id_LAN, ip_default, num_sonde))
    else:
        # Insert data into RESEAU_SONDE
        cursor.execute("INSERT INTO RESEAU_SONDE (num_sonde, id_LAN, ip_defaut_sonde) VALUES (%s, %s, %s)",
                       (num_sonde, id_LAN, ip_default))

    # Commit the changes
    conn.commit()

    ############################################################
    # Check if num_sonde exists in the COMMUNICATION_VPN table #
    ############################################################
    cursor.execute("SELECT num_sonde FROM COMMUNICATION_VPN WHERE num_sonde = %s", (num_sonde,))
    result = cursor.fetchone()
    if result:
        # Insert data into COMMUNICATION_VPN
        cursor.execute("UPDATE COMMUNICATION_VPN SET nom_tunnel = %s, adresse_client_VPN = %s WHERE num_sonde = %s",
                       (f'Tunnel 6', ip_vpn, num_sonde))
    else:
        # Insert data into COMMUNICATION_VPN
        cursor.execute("INSERT INTO COMMUNICATION_VPN (num_sonde, nom_tunnel, adresse_client_VPN) VALUES (%s, %s, %s)",
                       (num_sonde, f'Tunnel 6', ip_vpn))
    conn.commit()

    #############################################
    # Check if SIRET exists in the CLIENT table #
    #############################################
    cursor.execute("SELECT SIRET FROM CLIENT WHERE SIRET = %s", (siret_client,))
    result = cursor.fetchone()
    if result:
        # Insert data into client
        cursor.execute("UPDATE CLIENT SET nom_client = %s, adresse_client = %s WHERE SIRET = %s",
                       (nom_client, adresse_client, siret_client))
    else:
        # Insert data into client
        cursor.execute("INSERT INTO CLIENT (SIRET, nom_client, adresse_client) VALUES (%s, %s, %s)",
                       (siret_client, nom_client, adresse_client))
    conn.commit()

    print("Information has been successfully inserted into the database.")
except mysql.connector.Error as error:
    print(f"Error inserting data into the database: {error}")

# Close the database connection
cursor.close()
conn.close()
