# Pull next num sonde available if sembox-hostname is null
def pull_next_num_sonde(cursor, hostname):
    cursor.execute("SELECT num_sonde FROM SONDE WHERE nom_sonde = %s", (f"semabox-{hostname}",))
    result = cursor.fetchone()
    if result is not None:
        next_sonde_num = result[0]
    else:
        cursor.execute("SELECT MAX(num_sonde) FROM SONDE")
        result = cursor.fetchone()
        if result[0] is not None:
            next_sonde_num = result[0] + 1
        else:
            next_sonde_num = 1
    return next_sonde_num

# Pull next id_disk available 
def pull_next_id_disk(cursor):
    next_id_disk = 1
    cursor.execute("SELECT MAX(id_disque) FROM DISQUE_SEMA")
    result = cursor.fetchone()
    if result[0] is not None:
        next_id_disk = result[0] + 1
    return next_id_disk

# Pull next_id_semaos
def pull_next_id_semaos(cursor):
    next_id_semaos = 1
    cursor.execute("SELECT MAX(id_semaos) FROM SEMAOS")
    result = cursor.fetchone()
    if result[0] is not None:
        next_id_semaos = result[0] + 1
    return next_id_semaos

# Pull next_id_reseau
def pull_next_id_reseau(cursor):
    next_id_reseau = 1
    cursor.execute("SELECT MAX(id_LAN) FROM RESEAU_SONDE")
    result = cursor.fetchone()
    if result[0] is not None:
        next_id_reseau = result[0] + 1
    return next_id_reseau