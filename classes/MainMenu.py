#!/usr/bin/env python3

"""
Main Menu Class and Terminal Cursor Control

This module defines the `MainMenu` class, which represents the main menu of the application, and provides functions for controlling the terminal cursor.

Usage:
    Import and use the `MainMenu` class to display and interact with the main menu options.
    Use the provided functions to hide and show the terminal cursor.

Dependencies:
    - os: Module providing interaction with the operating system.
    - lib.style_menu: Module providing styling for the menu.
    - colorama: Module for adding color and style to terminal text.
    - click: Module for handling keyboard input and interactions.
    - lib.clear: Module providing functions to clear the terminal screen.

Classes:
    - MainMenu: Represents the main menu of the application.

Note:
    - This module is designed to be used as part of a larger application.
"""


import os
from lib.style_menu import style
from colorama import init, Fore, Back, Style
import click
from lib.clear import clear_screen

def hide_cursor():
    """
    Hide the terminal cursor.
    """
    click.echo("\033[?25l", nl=False)

def show_cursor():
    """
    Show the terminal cursor.
    """
    click.echo("\033[?25h", nl=False)  

class MainMenu:
    """
    Represents the main menu of the application.
    """
    def __init__(self):
        self.options = []
        self.options2 = []
        
    def main_menu(self):
        """
        Display and interact with the main menu options.
        
        Returns:
            tuple: A tuple containing the current choice index and the current list of options.
        """
        current_list = self.options
        current_choice = 0
        choice_made = False
        
        init() # Initialize colorama
        max_x, max_y = os.get_terminal_size()
        
        while not choice_made:
            hide_cursor()
            clear_screen()
            
            print(Fore.CYAN + Style.BRIGHT + style())
            print(Style.RESET_ALL)
            print(Fore.GREEN + "Use arrow keys to select an option")
            print("Press Enter to confirm\n")
            print("Choose an option:\n" + Fore.WHITE)
            
            for i, option in enumerate(self.options):
                if i == current_choice and current_list == self.options:
                    print(Fore.WHITE + "> " + Fore.CYAN + option + Style.RESET_ALL)
                else:
                    print("  " + option)
            click.echo(f'\033[{max_y-1};H')
            options_str = ""
            for i, option2 in enumerate(self.options2):
                if i == current_choice and current_list == self.options2:
                    options_str += Fore.WHITE + "> " + Fore.CYAN + option2 + " " + Style.RESET_ALL
                else:
                    options_str += Fore.WHITE + option2 + " " + Style.RESET_ALL

            click.echo(options_str, nl=False)                 
            key = click.getchar()

            if key == "\x1b[A":  # Up arrow
                if current_list == self.options:
                    current_choice -= 1
                    if current_choice < 0:
                        current_choice = len(self.options) - 1
                elif current_list == self.options2:
                    current_list = self.options
                    current_choice = 0
            elif key == "\x1b[B":  # Down arrow
                if current_list == self.options:
                    current_choice += 1
                    if current_choice == len(self.options):
                        current_choice = 0
                elif current_list == self.options2:
                    current_list = self.options
                    current_choice = 0
                    
            elif key == "\r":  # Enter
                choice_made = True
                return current_choice, current_list

            elif key == "\x1b[C":  # Right arrow
                if current_list == self.options:
                    current_list = self.options2
                    current_choice = 0
                elif current_list == self.options2:
                    current_choice += 1
                    if current_choice == len(self.options2):
                        current_choice = 0

            elif key == "\x1b[D":  # Left arrow
                if current_list == self.options2:
                    current_choice -= 1
                    if current_choice < 0:
                        current_choice = len(self.options2) - 1
                elif current_list == self.options:
                    current_list = self.options2
                    current_choice = 0
                
        return current_choice, current_list

    def main_exit(self):
        """Display and interact with the exit menu options.

        :return: int: The current choice index.
        """
        current_choice = 0
        choice_made = False
        max_x, max_y = os.get_terminal_size()
        
        init() # Initialize colorama
                
        while not choice_made:
            click.echo(f'\033[{max_y-1};H')
            options_str = ""
            
            for i, option in enumerate(self.options):
                if i == current_choice:
                    options_str += Fore.WHITE + "> " + Fore.CYAN + option + " " + Style.RESET_ALL
                else:
                    options_str += Fore.WHITE + option + " " + Style.RESET_ALL
                    
            click.echo(options_str, nl=False)
            key = click.getchar()

            if key == "\r":  # Enter
                choice_made = True
                return current_choice

            elif key == "\x1b[C":  # Right arrow
                current_choice += 1
                if current_choice == len(self.options):
                    current_choice = 0

            elif key == "\x1b[D":  # Left arrow
                current_choice -= 1
                if current_choice < 0:
                    current_choice = len(self.options) - 1

        return current_choice
