#!/usr/bin/env python3

"""
Semabox Deployment Script

This script provides a graphical user interface (GUI) for deploying and configuring Semabox. It utilizes the 'tkinter' library for creating interactive windows, buttons, and input fields. The script offers options to install various services and perform Semabox-related actions.

Usage:
    Run this script using Python to launch the Semabox deployment GUI.

Dependencies:
    - tkinter: Python's standard GUI library for creating graphical interfaces.

Modules:
    - os: Module providing interaction with the operating system.
    - tkinter: Module for creating graphical user interfaces.
    - lib.progress: Module for executing tasks with progress tracking.

Note:
    - Ensure the required Ansible playbooks and other resources are present.
    - This script requires the 'tkinter' library, which is included with standard Python installations.
"""


import subprocess
import os
from tkinter import Tk, Button, Label, Entry, Frame, Listbox, Checkbutton, IntVar, BOTTOM, LEFT, RIGHT
from tkinter import ttk
import tkinter


def deploy_semabox():
    """
    - Displays options for deploying Semabox.
    """
    button_install_semabox.pack_forget()
    button_service_semabox.pack_forget()
    button_quit.pack_forget()
    listbox.pack_forget()

    # Afficher les nouveaux boutons et la liste de choix
    listbox_host_options.pack()
    button_semabox_install.pack(side=RIGHT, anchor="e", padx=10, pady=5)

    button_return.pack(side=LEFT, padx=10, pady=5)
def install_vim():
    """
    - Installs 'vim' using the system package manager.
    """
    os.system("sudo apt install -y vim")


def install_net_tools():
    """
    - Installs 'net-tools' using the system package manager.
    """
    os.system("bash -c apt-get update && apt-get install -y net-tools")


def install_tcpdump():
    """
    - Installs 'tcpdump' using the system package manager.
    """
    os.system("bash -c apt-get update && apt-get install -y tcpdump")


def install_nmap():
    """
    - Installs 'nmap' using the system package manager.
    """
    os.system("bash -c apt-get update && apt-get install -y nmap")


def quit_application():
    """
    - Exits the application.
    """
    fenetre.destroy()


def display_service_options():
    """
    - Displays options for installing various services.
    """
    button_install_semabox.pack_forget()
    button_service_semabox.pack_forget()
    button_quit.pack_forget()
    listbox.pack_forget()

    # Afficher les nouveaux boutons et la liste de choix
    listbox_service_options.pack()
    button_service_install.pack(side=RIGHT, anchor="e", padx=10, pady=5)
    button_return.pack(side=LEFT, padx=10, pady=5)


def execute_service_action():
    """
    - Executes actions based on selected service options.
    """
    selected_items = [item.get() for item in checkbox_vars]
    
    # Afficher le texte "Installation en cours"
    installation_label = tkinter.Label(main_frame, text="Installation en cours", font=("Arial", 12, "bold"))
    installation_label.pack(pady=10)
    pb = ttk.Progressbar(main_frame, orient='horizontal', mode='determinate', length=280)
    pb.pack(pady=10)
    
    # Exécuter l'action en fonction des éléments sélectionnés
    if selected_items[0] != 0:
        install_vim()
    elif selected_items[1] != 0:
        install_net_tools()
    elif selected_items[2] != 0:
        install_tcpdump()
    elif selected_items[3] != 0:
        install_nmap()
    elif selected_items[4] != 0:
        pb.start()
        # Exécuter le subprocess dans une boucle while
        process = subprocess.Popen(
            ["docker", "exec", "-i", "my_semabox", "bash", "-c", "python3 gui_bar_progress.py --path_ansible ./ansible/playbooks/deploy_zabbix.yml --total 74"])
        while process.poll() is None:
            pb.update()
        pb.stop()
        os.system("clear")
        print("GUI-Interface Running")
        pb.pack_forget()
        installation_label.pack_forget()
        show_main_menu()

def execute_semabox_action():
    """
    - Executes actions based on selected Semabox options.
    """
    selected_items = [item.get() for item in host_vars]

    if selected_items[0] != 0:
        
        def define_lan():
            def execute_subprocess():
                entry_ip_srv_cma_zabbix.pack_forget()
                entry_ip_srv_cma_zabbix_label.pack_forget()
                entry_entreprise.pack_forget()
                entry_entreprise_label.pack_forget()
                entry_address.pack_forget()
                entry_address_label.pack_forget()
                entry_presta.pack_forget()
                entry_presta_label.pack_forget()
                entry_siret_label.pack_forget()
                entry_siret.pack_forget()
                entry_name_subnet.pack_forget()
                entry_name_subnet_label.pack_forget()
                entry_subnet.pack_forget()
                entry_subnet_label.pack_forget()
                button_execute.pack_forget()
                button_return.pack_forget()
                listbox_host_options.pack_forget()

                # Afficher le texte "Installation en cours"
                installation_label = tkinter.Label(main_frame, text="Installation en cours", font=("Arial", 12, "bold"))
                installation_label.pack(pady=10)
                pb = ttk.Progressbar(main_frame, orient='horizontal', mode='determinate', length=280)
                pb.pack(pady=10)

                # Obtenir les valeurs saisies par l'utilisateur
                ip_srv_cma_zabbix = entry_ip_srv_cma_zabbix.get()
                entreprise = entry_entreprise.get()
                address_entreprise = entry_address.get()
                siret = entry_siret.get()
                presta = entry_presta.get()
                name_subnet = entry_name_subnet.get()
                subnet = entry_subnet.get()
                old_ip_priv_cma_zabbix = os.system("grep 'semabox_ip_priv =' ansible/roles/hosts | awk -F'semabox_ip_priv =' '{print $2}'")
                                
                os.system(f'sed -i "s/line: \"Server={old_ip_priv_cma_zabbix}\"/line: \"Server={ip_srv_cma_zabbix}\"/g" ansible/roles/semabox/tasks/main.yml')
                os.system(f'sed -i "s/line: \"ServerActive={old_ip_priv_cma_zabbix}\"/line: \"ServerActive={ip_srv_cma_zabbix}\"/g" ansible/roles/semabox/tasks/main.yml')
                # Exécuter les commandes Docker
                subprocess.run(["docker", "rm", "-f", "my_semabox"])
                subprocess.run(["docker", "rmi", "my_semabox"])
                subprocess.run(["docker", "build", "-t", "my_semabox", "."])
                subprocess.run(["docker", "network", "create", f"--subnet={subnet}", f"{name_subnet}"])
                subprocess.run(["docker", "run", "-dit", "--name", "my_semabox", f"--network={name_subnet}", "my_semabox"])
                subprocess.run(["docker", "start", "my_semabox"])
                os.system("clear")
                # Écrire les valeurs dans le fichier secret
                subprocess.run(["docker", "exec", "-i", "my_semabox", "bash", "-c", f"echo 'Nom Entreprise: {entreprise}' > secret"])
                subprocess.run(["docker", "exec", "-i", "my_semabox", "bash", "-c", f"echo 'Adresse Entreprise: {address_entreprise}' > secret"])
                subprocess.run(["docker", "exec", "-i", "my_semabox", "bash", "-c", f"echo 'Siret Entreprise: {siret}' >> secret"])
                subprocess.run(["docker", "exec", "-i", "my_semabox", "bash", "-c", f"echo 'Nom Prestataire: {presta}' >> secret"])

                pb.start()
                # Exécuter le subprocess dans une boucle while
                process = subprocess.Popen(
                    ["docker", "exec", "-i", "my_semabox", "bash", "-c", "python3 gui_bar_progress.py --path_ansible ./ansible/playbooks/deploy_semabox.yml --total 38"])
                while process.poll() is None:
                    pb.update()
                pb.stop()
                os.system("clear")
                print("GUI-Interface Running")
                pb.pack_forget()
                installation_label.pack_forget()
                show_main_menu()
            
            entry_entreprise.pack_forget()
            entry_entreprise_label.pack_forget()
            entry_address.pack_forget()
            entry_address_label.pack_forget()
            entry_presta.pack_forget()
            entry_presta_label.pack_forget()
            entry_siret_label.pack_forget()
            entry_siret.pack_forget()
            button_return.pack_forget()
            listbox_host_options.pack_forget()
            button_exec.pack_forget()
            
            # Afficher les champs d'entrée (entrypoints)
            entry_name_subnet_label = Label(main_frame, text="Name Subnet:")
            entry_name_subnet_label.pack(pady=5)
            entry_name_subnet = Entry(main_frame)
            entry_name_subnet.pack(pady=5)
            
            entry_subnet_label = Label(main_frame, text="Subnet: x.x.x.x/x")
            entry_subnet_label.pack(pady=5)
            entry_subnet = Entry(main_frame)
            entry_subnet.pack(pady=5)
            
            # Bouton pour exécuter le subprocess
            button_execute = Button(main_frame, text="Valider", command=execute_subprocess)
            button_execute.pack(pady=15)
                
        # Masquer les boutons et la liste
        button_install_semabox.pack_forget()
        button_service_semabox.pack_forget()
        button_semabox_install.pack_forget()
        button_quit.pack_forget()
        button_return.pack_forget()
        listbox.pack_forget()
        listbox_host_options.pack_forget()

        # Afficher les champs d'entrée (entrypoints)
        entry_ip_srv_cma_zabbix_label = Label(main_frame, text="ip_srv_cma_zabbix:")
        entry_ip_srv_cma_zabbix_label.pack(pady=5)
        entry_ip_srv_cma_zabbix = Entry(main_frame)
        entry_ip_srv_cma_zabbix.pack(pady=5)
        
        entry_entreprise_label = Label(main_frame, text="Nom Entreprise:")
        entry_entreprise_label.pack(pady=5)
        entry_entreprise = Entry(main_frame)
        entry_entreprise.pack(pady=5)

        entry_address_label = Label(main_frame, text="Adresse Entreprise:")
        entry_address_label.pack(pady=5)
        entry_address = Entry(main_frame)
        entry_address.pack(pady=5)

        entry_siret_label = Label(main_frame, text="Siret Entreprise:")
        entry_siret_label.pack(pady=5)
        entry_siret = Entry(main_frame)
        entry_siret.pack(pady=5)

        entry_presta_label = Label(main_frame, text="Nom Prestataire:")
        entry_presta_label.pack(pady=5)
        entry_presta = Entry(main_frame)
        entry_presta.pack(pady=5)

        # Bouton pour exécuter le subprocess
        button_exec = Button(main_frame, text="Valider", command=define_lan)
        button_exec.pack(pady=15)

        # Barre de progression
        pb = ttk.Progressbar(fenetre, orient='horizontal', mode='determinate', length=280)
        pb.pack(side=BOTTOM, padx=10, pady=5)

    elif selected_items[1] != 0:
        pass
    elif selected_items[2] != 0:
        pass
    pb.pack_forget()


def show_main_menu():
    """
    - show_main_menu(): Displays the main menu.
    """
    listbox_service_options.pack_forget()
    listbox_host_options.pack_forget()
    button_service_install.pack_forget()
    button_semabox_install.pack_forget()
    button_return.pack_forget()
    button_quit.pack(side=LEFT, padx=10, pady=5)
    button_install_semabox.pack(side=LEFT, padx=10, pady=5)
    button_service_semabox.pack(side=LEFT, padx=10, pady=5)


if __name__ == "__main__":
    fenetre = Tk()
    fenetre.geometry("640x480")
    fenetre.title("Semabox")

    title_frame = Frame(fenetre)
    title_frame.pack()
    main_frame = Frame(fenetre)
    main_frame.pack(expand=True, pady=5)
    button_frame = Frame(main_frame)
    button_frame.pack()

    title_label = Label(title_frame, text="Semabox", relief="solid", borderwidth=2, font=("Arial", 16, "bold"))
    title_label.pack(padx=10, pady=25)

    button_install_semabox = Button(button_frame, text="Install Semabox", command=deploy_semabox)
    button_install_semabox.pack(side=LEFT, padx=10, pady=5)
    button_semabox_install = Button(fenetre, text="Install", command=execute_semabox_action)
    button_semabox_install.pack(side=RIGHT, anchor="e", padx=10, pady=5)

    button_service_semabox = Button(button_frame, text="Services Semabox", command=display_service_options)
    button_service_semabox.pack(side=LEFT, padx=10, pady=5)
    button_service_install = Button(fenetre, text="Install", command=execute_service_action)
    button_service_install.pack(side=RIGHT, anchor="e", padx=10, pady=5)

    button_quit = Button(fenetre, text="Quitter", command=quit_application)
    button_quit.pack(side=LEFT, padx=10, pady=5)
    button_return = Button(fenetre, text="Return", command=show_main_menu)
    button_return.pack(side=LEFT, padx=10, pady=5)

    button_service_install.pack_forget()
    button_return.pack_forget()
    button_semabox_install.pack_forget()

    listbox = Listbox(main_frame)
    listbox_service_options = Frame(main_frame)
    listbox_host_options = Frame(main_frame)

    checkbox_vars = []
    service_options = ["Vim", "Net-tools", "TcpDump", "Nmap", "Zabbix"]
    host_vars = []
    host_options = ["Docker", "Localhost", "SSH"]

    for option in service_options:
        var = IntVar()
        checkbox = Checkbutton(listbox_service_options, text=option, variable=var)
        checkbox.pack(anchor="w")
        checkbox_vars.append(var)

    for option in host_options:
        var = IntVar()
        checkbox = Checkbutton(listbox_host_options, text=option, variable=var)
        checkbox.pack(anchor="w")
        host_vars.append(var)

    fenetre.grid_rowconfigure(1, weight=1)
    fenetre.grid_columnconfigure(0, weight=1)
    main_frame.grid_rowconfigure(1, weight=1)

    fenetre.mainloop()
    os.system("clear")
