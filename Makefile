.PHONY: help install-dependencies-docker install-dependencies-ubuntu run_cli run_gui

help:
	@echo   "Available targets:"
	@echo   " install-dependencies-docker    Install dependencies for Docker"
	@echo   " install-dependencies-ubuntu    Install dependencies for Ubuntu"
	@echo   " run_cli                        Run the CLI"
	@echo   " run_gui                        Run the GUI"

install-dependencies-docker:
	sudo docker build -t my_sema_ubuntu ubuntu
	sudo docker build -t zabuntu zabbix/zabuntu
	pip3 install --user -r requirements.txt

install-dependencies-ubuntu:
	# Installer Docker
	sudo apt-get update
	sudo apt-get install -y docker.io

	# Installer Python 3, pip3, wget et unzip
	sudo apt-get install -y python3 python3-pip wget unzip
	cd /
	wget https://gitlab.com/epsipoei/semabox/-/archive/main/semabox-main.zip   
	unzip semabox-main.zip   
	mv semabox-main semabox   
	chmod 755 -R semabox

	pip3 install --user -r requirements.txt

run_cli:
	python3 main.py

run_gui:
	python3 main_gui.py

show_docs:
	firefox docs/_build/html/index.html &