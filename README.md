<!-- markdownlint-disable-file MD033 MD041-->
<div align="center">

# SEMABOX

</div>

Semabox is an application that allows you to deploy and manage various services and configurations using both a Command Line Interface (CLI) and a Graphical User Interface (GUI).

## **Summary**

[[*TOC*]]


### **Prerequisites**

- For Ubuntu

To install the required dependencies on Ubuntu, run:

    make install-dependencies-ubuntu

- For Docker

To install the required dependencies using Docker, run:

    make install-dependencies-docker

### **Usage**

- Run Semabox CLI

To run the Semabox Command Line Interface (CLI), execute:

    make run_cli

- Run Semabox GUI

To run the Semabox Graphical User Interface (GUI), execute

    make run_gui

### **Documentation**

To view the documentation, run

    make show_docs

### **Structure**

The Semabox project consists of several files and directories that fulfill specific functions. Here are the main components of the project:

| Files | Description |
|:-----------|:-----------|
| semabox/cli.py | Provides a command line interface to execute Semabox tasks |
| semabox/main_ui.py | Implements the main graphical user interface of the Semabox application |
|semabox/main.py | Serves as the entry point for executing Semabox tasks |
|semabox/ansible/roles/semabox/tasks/main.yml | Contains Ansible tasks for configuring the Semabox environment |
|semabox/ansible/roles/zabbix/tasks/main.yml | Contains Ansible tasks for deploying and configuring Zabbix monitoring |
|semabox/classes/MainMenu.py | Defines a class MainMenu that manages the main menu's user interface |
|semabox/lib/style_menu.py | Provides a function style() to generate a stylized title and version information |
|semabox/lib/progress.py | Contains a function execute_with_progress() to execute tasks with progress tracking |
|semabox/lib/menu.py | Contains functions for navigating through the application's menus |

### **Dependencies**

The project depends on the Click library for CLI functionality.  
The GUI is built using the Tkinter library for user interface components.  
Ansible is used for configuration management tasks.  
The tqdm library is used for displaying progress bars.

### **Future Improvements**

Future enhancements to the Semabox project could include error handling, improved user experience, and extending the range of supported actions and services.  

### **Conclusion**

The Semabox project provides an intuitive way to deploy and manage services and configurations using both CLI and GUI interfaces. Its modular structure and use of Ansible for configuration management make it a flexible tool for various tasks.