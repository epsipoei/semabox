Classes
=======


MainMenu
--------

.. toctree::
   :maxdepth: 1

   classes_code

.. automodule:: classes.MainMenu
   :members:
   :undoc-members:
   :show-inheritance: