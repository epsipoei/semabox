cli module
==========

.. toctree::
   :maxdepth: 1

   cli_code

.. automodule:: cli
   :members:
   :undoc-members:
   :show-inheritance:
