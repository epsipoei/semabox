Ansible
=======

Role semabox
------------

.. literalinclude:: ../ansible/roles/semabox/tasks/main.yml
   :language: yaml


Role zabbix
-----------

.. literalinclude:: ../ansible/roles/zabbix/tasks/main.yml
   :language: yaml