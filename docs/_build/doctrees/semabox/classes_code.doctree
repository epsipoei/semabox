���9      �sphinx.addnodes��document���)��}�(�	rawsource�� ��children�]��docutils.nodes��section���)��}�(hhh]�(h	�title���)��}�(h�MainMenu.py�h]�h	�Text����MainMenu.py�����}�(�parent�h�	_document�h�source�N�line�Nuba�
attributes�}�(�ids�]��classes�]��names�]��dupnames�]��backrefs�]�u�tagname�hhhhhh�F/media/kevin/my_data/mes_scripts/semabox/docs/semabox/classes_code.rst�hKubh	�literal_block���)��}�(hX�  #!/usr/bin/env python3

"""
Main Menu Class and Terminal Cursor Control

This module defines the `MainMenu` class, which represents the main menu of the application, and provides functions for controlling the terminal cursor.

Usage:
    Import and use the `MainMenu` class to display and interact with the main menu options.
    Use the provided functions to hide and show the terminal cursor.

Dependencies:
    - os: Module providing interaction with the operating system.
    - lib.style_menu: Module providing styling for the menu.
    - colorama: Module for adding color and style to terminal text.
    - click: Module for handling keyboard input and interactions.
    - lib.clear: Module providing functions to clear the terminal screen.

Classes:
    - MainMenu: Represents the main menu of the application.

Note:
    - This module is designed to be used as part of a larger application.
"""


import os
from lib.style_menu import style
from colorama import init, Fore, Back, Style
import click
from lib.clear import clear_screen

def hide_cursor():
    """
    Hide the terminal cursor.
    """
    click.echo("\033[?25l", nl=False)

def show_cursor():
    """
    Show the terminal cursor.
    """
    click.echo("\033[?25h", nl=False)  

class MainMenu:
    """
    Represents the main menu of the application.
    """
    def __init__(self):
        self.options = []
        self.options2 = []
        
    def main_menu(self):
        """
        Display and interact with the main menu options.
        
        Returns:
            tuple: A tuple containing the current choice index and the current list of options.
        """
        current_list = self.options
        current_choice = 0
        choice_made = False
        
        init() # Initialize colorama
        max_x, max_y = os.get_terminal_size()
        
        while not choice_made:
            hide_cursor()
            clear_screen()
            
            print(Fore.CYAN + Style.BRIGHT + style())
            print(Style.RESET_ALL)
            print(Fore.GREEN + "Use arrow keys to select an option")
            print("Press Enter to confirm\n")
            print("Choose an option:\n" + Fore.WHITE)
            
            for i, option in enumerate(self.options):
                if i == current_choice and current_list == self.options:
                    print(Fore.WHITE + "> " + Fore.CYAN + option + Style.RESET_ALL)
                else:
                    print("  " + option)
            click.echo(f'\033[{max_y-1};H')
            options_str = ""
            for i, option2 in enumerate(self.options2):
                if i == current_choice and current_list == self.options2:
                    options_str += Fore.WHITE + "> " + Fore.CYAN + option2 + " " + Style.RESET_ALL
                else:
                    options_str += Fore.WHITE + option2 + " " + Style.RESET_ALL

            click.echo(options_str, nl=False)                 
            key = click.getchar()

            if key == "\x1b[A":  # Up arrow
                if current_list == self.options:
                    current_choice -= 1
                    if current_choice < 0:
                        current_choice = len(self.options) - 1
                elif current_list == self.options2:
                    current_list = self.options
                    current_choice = 0
            elif key == "\x1b[B":  # Down arrow
                if current_list == self.options:
                    current_choice += 1
                    if current_choice == len(self.options):
                        current_choice = 0
                elif current_list == self.options2:
                    current_list = self.options
                    current_choice = 0
                    
            elif key == "\r":  # Enter
                choice_made = True
                return current_choice, current_list

            elif key == "\x1b[C":  # Right arrow
                if current_list == self.options:
                    current_list = self.options2
                    current_choice = 0
                elif current_list == self.options2:
                    current_choice += 1
                    if current_choice == len(self.options2):
                        current_choice = 0

            elif key == "\x1b[D":  # Left arrow
                if current_list == self.options2:
                    current_choice -= 1
                    if current_choice < 0:
                        current_choice = len(self.options2) - 1
                elif current_list == self.options:
                    current_list = self.options2
                    current_choice = 0
                
        return current_choice, current_list

    def main_exit(self):
        """Display and interact with the exit menu options.

        :return: int: The current choice index.
        """
        current_choice = 0
        choice_made = False
        max_x, max_y = os.get_terminal_size()
        
        init() # Initialize colorama
                
        while not choice_made:
            click.echo(f'\033[{max_y-1};H')
            options_str = ""
            
            for i, option in enumerate(self.options):
                if i == current_choice:
                    options_str += Fore.WHITE + "> " + Fore.CYAN + option + " " + Style.RESET_ALL
                else:
                    options_str += Fore.WHITE + option + " " + Style.RESET_ALL
                    
            click.echo(options_str, nl=False)
            key = click.getchar()

            if key == "\r":  # Enter
                choice_made = True
                return current_choice

            elif key == "\x1b[C":  # Right arrow
                current_choice += 1
                if current_choice == len(self.options):
                    current_choice = 0

            elif key == "\x1b[D":  # Left arrow
                current_choice -= 1
                if current_choice < 0:
                    current_choice = len(self.options) - 1

        return current_choice
�h]�hX�  #!/usr/bin/env python3

"""
Main Menu Class and Terminal Cursor Control

This module defines the `MainMenu` class, which represents the main menu of the application, and provides functions for controlling the terminal cursor.

Usage:
    Import and use the `MainMenu` class to display and interact with the main menu options.
    Use the provided functions to hide and show the terminal cursor.

Dependencies:
    - os: Module providing interaction with the operating system.
    - lib.style_menu: Module providing styling for the menu.
    - colorama: Module for adding color and style to terminal text.
    - click: Module for handling keyboard input and interactions.
    - lib.clear: Module providing functions to clear the terminal screen.

Classes:
    - MainMenu: Represents the main menu of the application.

Note:
    - This module is designed to be used as part of a larger application.
"""


import os
from lib.style_menu import style
from colorama import init, Fore, Back, Style
import click
from lib.clear import clear_screen

def hide_cursor():
    """
    Hide the terminal cursor.
    """
    click.echo("\033[?25l", nl=False)

def show_cursor():
    """
    Show the terminal cursor.
    """
    click.echo("\033[?25h", nl=False)  

class MainMenu:
    """
    Represents the main menu of the application.
    """
    def __init__(self):
        self.options = []
        self.options2 = []
        
    def main_menu(self):
        """
        Display and interact with the main menu options.
        
        Returns:
            tuple: A tuple containing the current choice index and the current list of options.
        """
        current_list = self.options
        current_choice = 0
        choice_made = False
        
        init() # Initialize colorama
        max_x, max_y = os.get_terminal_size()
        
        while not choice_made:
            hide_cursor()
            clear_screen()
            
            print(Fore.CYAN + Style.BRIGHT + style())
            print(Style.RESET_ALL)
            print(Fore.GREEN + "Use arrow keys to select an option")
            print("Press Enter to confirm\n")
            print("Choose an option:\n" + Fore.WHITE)
            
            for i, option in enumerate(self.options):
                if i == current_choice and current_list == self.options:
                    print(Fore.WHITE + "> " + Fore.CYAN + option + Style.RESET_ALL)
                else:
                    print("  " + option)
            click.echo(f'\033[{max_y-1};H')
            options_str = ""
            for i, option2 in enumerate(self.options2):
                if i == current_choice and current_list == self.options2:
                    options_str += Fore.WHITE + "> " + Fore.CYAN + option2 + " " + Style.RESET_ALL
                else:
                    options_str += Fore.WHITE + option2 + " " + Style.RESET_ALL

            click.echo(options_str, nl=False)                 
            key = click.getchar()

            if key == "\x1b[A":  # Up arrow
                if current_list == self.options:
                    current_choice -= 1
                    if current_choice < 0:
                        current_choice = len(self.options) - 1
                elif current_list == self.options2:
                    current_list = self.options
                    current_choice = 0
            elif key == "\x1b[B":  # Down arrow
                if current_list == self.options:
                    current_choice += 1
                    if current_choice == len(self.options):
                        current_choice = 0
                elif current_list == self.options2:
                    current_list = self.options
                    current_choice = 0
                    
            elif key == "\r":  # Enter
                choice_made = True
                return current_choice, current_list

            elif key == "\x1b[C":  # Right arrow
                if current_list == self.options:
                    current_list = self.options2
                    current_choice = 0
                elif current_list == self.options2:
                    current_choice += 1
                    if current_choice == len(self.options2):
                        current_choice = 0

            elif key == "\x1b[D":  # Left arrow
                if current_list == self.options2:
                    current_choice -= 1
                    if current_choice < 0:
                        current_choice = len(self.options2) - 1
                elif current_list == self.options:
                    current_list = self.options2
                    current_choice = 0
                
        return current_choice, current_list

    def main_exit(self):
        """Display and interact with the exit menu options.

        :return: int: The current choice index.
        """
        current_choice = 0
        choice_made = False
        max_x, max_y = os.get_terminal_size()
        
        init() # Initialize colorama
                
        while not choice_made:
            click.echo(f'\033[{max_y-1};H')
            options_str = ""
            
            for i, option in enumerate(self.options):
                if i == current_choice:
                    options_str += Fore.WHITE + "> " + Fore.CYAN + option + " " + Style.RESET_ALL
                else:
                    options_str += Fore.WHITE + option + " " + Style.RESET_ALL
                    
            click.echo(options_str, nl=False)
            key = click.getchar()

            if key == "\r":  # Enter
                choice_made = True
                return current_choice

            elif key == "\x1b[C":  # Right arrow
                current_choice += 1
                if current_choice == len(self.options):
                    current_choice = 0

            elif key == "\x1b[D":  # Left arrow
                current_choice -= 1
                if current_choice < 0:
                    current_choice = len(self.options) - 1

        return current_choice
�����}�hh/sbah}�(h!]�h#]�h%]�h']�h)]��source��</media/kevin/my_data/mes_scripts/semabox/classes/MainMenu.py��	xml:space��preserve��force���language��python��highlight_args�}��linenostart�Ksuh+h-hh,hKhhhhubeh}�(h!]��mainmenu-py�ah#]�h%]��mainmenu.py�ah']�h)]�uh+h
hhhhhh,hKubah}�(h!]�h#]�h%]�h']�h)]��source�h,uh+h�current_source�N�current_line�N�settings��docutils.frontend��Values���)��}�(hN�	generator�N�	datestamp�N�source_link�N�
source_url�N�toc_backlinks��entry��footnote_backlinks�K�sectnum_xform�K�strip_comments�N�strip_elements_with_classes�N�strip_classes�N�report_level�K�
halt_level�K�exit_status_level�K�debug�N�warning_stream�N�	traceback���input_encoding��	utf-8-sig��input_encoding_error_handler��strict��output_encoding��utf-8��output_encoding_error_handler�hr�error_encoding��utf-8��error_encoding_error_handler��backslashreplace��language_code��en��record_dependencies�N�config�N�	id_prefix�h�auto_id_prefix��id��dump_settings�N�dump_internals�N�dump_transforms�N�dump_pseudo_xml�N�expose_internals�N�strict_visitor�N�_disable_config�N�_source�h,�_destination�N�_config_files�]��file_insertion_enabled���raw_enabled�K�line_length_limit�M'�pep_references�N�pep_base_url��https://peps.python.org/��pep_file_url_template��pep-%04d��rfc_references�N�rfc_base_url��&https://datatracker.ietf.org/doc/html/��	tab_width�K�trim_footnote_reference_space���syntax_highlight��long��smart_quotes���smartquotes_locales�]��character_level_inline_markup���doctitle_xform���docinfo_xform�K�sectsubtitle_xform���image_loading��link��embed_stylesheet���cloak_email_addresses���section_self_link���env�Nub�reporter�N�indirect_targets�]��substitution_defs�}��substitution_names�}��refnames�}��refids�}��nameids�}�hLhIs�	nametypes�}�hL�sh!}�hIhs�footnote_refs�}��citation_refs�}��autofootnotes�]��autofootnote_refs�]��symbol_footnotes�]��symbol_footnote_refs�]��	footnotes�]��	citations�]��autofootnote_start�K�symbol_footnote_start�K �
id_counter��collections��Counter���}���R��parse_messages�]��transform_messages�]��transformer�N�include_log�]��
decoration�Nhhub.