import os
from classes.MainMenu import MainMenu


def return_or_exit():
    """
    Create and display a submenu with options to return to the main menu or exit the application.

    Returns:
        int: The selected menu option.
    """
    self = MainMenu()
    self.options = ['Return Main-Menu', 'Exit']
    self.options2 = []
    menu = MainMenu.main_exit(self)
    
    return menu

def main_menu():
    """
    Display the main menu and return the selected option.

    Returns:
        int: The selected menu option.
    """
    self = MainMenu()
    self.options = ['Install Semabox', 'Services Semabox']
    self.options2 = ['Exit']
    menu = MainMenu.main_menu(self)
    current_choise = menu[0]
    current_list = menu[1]

    if current_list == self.options:
        os.system("clear")
        return current_choise
    if current_list == self.options2:
        os.system("clear")
        raise SystemExit()
        
    return current_choise

def main_choice():
    """
    Display the main menu and return the selected option.

    Returns:
        int: The selected menu option.
    """
    self = MainMenu()
    self.options = ['Install on Docker', 'Install on localhost']
    self.options2 = ['Exit']
    menu = MainMenu.main_menu(self)
    current_choise = menu[0]
    current_list = menu[1]

    if current_list == self.options:
        os.system("clear")
        return current_choise
    if current_list == self.options2:
        os.system("clear")
        raise SystemExit()
        
    return current_choise

def main_semabox():
    """
    Display the Semabox options menu and return the selected option.

    Returns:
        int: The selected menu option.
    """
    self = MainMenu()
    self.options = ['vim', 'net-tools', 'tcpdump', 'nmap', 'Serveur-Zabbix']
    self.options2 = ['Return', 'Exit']
    menu = MainMenu.main_menu(self)
    current_choise = menu[0]
    current_list = menu[1]
    
    if current_list == self.options:
        return current_choise
    if current_list == self.options2:
        if current_choise == 0:
            os.system("python3 ./main.py")
        if current_choise == 1:
            os.system("clear")
            raise SystemExit()    

    return current_choise

def main_zabbix():
    """
    Display the Zabbix options menu and return the selected option.

    Returns:
        int: The selected menu option.
    """
    self = MainMenu()
    self.options = ['Installer Zabbix', 'Supprimer Zabbix']
    self.options2 = ['Return', 'Exit']

    menu = MainMenu.main_menu(self)
    current_choise = menu[0]
    current_list = menu[1]
    
    if current_list == self.options:
        return current_choise
    if current_list == self.options2:
        if current_choise == 0:
            os.system("python3 ./main.py")
        if current_choise == 1:
            os.system("clear")
            raise SystemExit()    

    return current_choise

