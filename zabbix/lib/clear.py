import os

def clear_screen():
    """
    Clear the terminal screen based on the operating system.

    This function checks the operating system and uses the appropriate command to clear the screen.
    For Unix-based systems (Linux, macOS, etc.), it uses "clear" command.
    For Windows systems, it uses "cls" command.
    If the operating system is not recognized, it prints an error message.

    Parameters:
        None

    Returns:
        None
    """
    if os.name == "posix":
        # For Unix-based systems (Linux, macOS, etc.)
        os.system("clear")
    elif os.name in ("nt", "dos", "ce"):
        # For Windows systems
        os.system("cls")
    else:
        # If the operating system is not recognized, print an error message
        print("The command to clear the screen is not available for this operating system.")
