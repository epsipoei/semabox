import itertools
import re
import subprocess
import time
import os
import click
from lib.style_menu import style
from colorama import Fore, Back, Style
from tqdm import tqdm


def hide_cursor():
    """
    Hide the terminal cursor.
    """
    click.echo("\033[?25l", nl=False)
def show_cursor():
    """
    Show the terminal cursor.
    """
    click.echo("\033[?25h", nl=False) 

def execute_with_progress(script_path: str, total: int):
    """
    Execute a command with a progress bar.

    Parameters:
        script_path (str): Path to the script or playbook to execute.
        total (int): Total steps or units of progress.

    Returns:
        None
    """
    hide_cursor()
    os.system('clear')
    # Set up the progress bar
    progress = 0
    # Define the animation
    animation = itertools.cycle(['/',u'\u2500','\\','|'])

    try:

        with subprocess.Popen(["ansible-playbook", "-i", "./ansible/inventory", "-b", script_path],
                            stdout=subprocess.PIPE,
                            bufsize=1,
                            universal_newlines=True) as p:
    
            # Print the title and version
            print(Fore.CYAN + Style.BRIGHT + style())
            print(Style.RESET_ALL)

            print(Fore.CYAN + Style.BRIGHT + "\n" + "="*16 + " Playbook Running " + "="*16)
            print(Style.RESET_ALL)

            pbar1=tqdm(p.stdout,
                    total=total,
                    bar_format= Fore.YELLOW + " "*14 + "{desc}",
                    ncols=50,
                    colour="CYAN")

            pbar2=tqdm(p.stdout,
                    total=total,
                    bar_format= Fore.YELLOW + "{percentage:3.0f}% |{bar}" + Fore.YELLOW + "|{n_fmt}/{total_fmt}",
                    ncols=50,
                    colour="CYAN")

            for line in p.stdout:
                progress += 1
                pbar1.update(1)
                pbar2.update(1)
                task_name_match = re.search(r'TASK \[(.*)\]', line)
                if task_name_match:
                    task_name = task_name_match.group(1)
                    pbar1.set_description(task_name)
                animation_frame = next(animation)
                print(Fore.YELLOW + f"\rLoading...[{animation_frame}]", end='')
                time.sleep(0.1)
        pbar1.close()
        pbar2.close()
        p.wait()        
        print(Fore.GREEN + Style.BRIGHT + "\n\n" + "="*9 + " Playbook executed successfully " + "="*9)

    finally:   
        show_cursor()


