#!/usr/bin/env python3


"""
Script for running a command-line progress bar using the 'execute_with_progress' function from the 'lib.progress' module.
Uses the 'click' library for command-line option handling.

Usage:
    Run this script using Python 3 to execute a command with a progress bar.
"""

import click
from lib.progress import execute_with_progress

@click.command()
@click.option("--path_ansible", type=str, required=True, help="Path to Ansible")
@click.option("--total", type=int, required=True, help="Number of tasks")

def bar_progress(path_ansible, total):
    """
    Run a command using the 'execute_with_progress' function with a progress bar.

    Parameters:
        path_ansible (str): Path to the Ansible playbook to be executed.

    Returns:
        None
    """
    execute_with_progress(path_ansible, total)

if __name__ == "__main__":
    bar_progress()