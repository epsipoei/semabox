#!/usr/bin/env python3

from pyzabbix import ZabbixAPI

# Remplacez par vos propres informations d'identification et URL
zabbix_url = "http://192.168.2.2:8080"
zabbix_username = "Admin"
zabbix_password = "zabbix"

zapi = ZabbixAPI(zabbix_url)
zapi.login(zabbix_username, zabbix_password)

hosts = {
    "mysql_master": "10.10.10.3",
    "mysql_slave": "10.10.10.4"
}
group_name = "Databases"  # Nom du groupe d'hôtes
template_names = ["MySQL by Zabbix agent", "Linux by Zabbix agent"]  # Noms des modèles

# Obtenez l'ID du groupe d'hôtes
group_id = zapi.hostgroup.get(filter={"name": group_name})[0]["groupid"]

# Obtenez les ID des modèles
template_ids = [zapi.template.get(filter={"host": template_name})[0]["templateid"] for template_name in template_names]

for host_name, host_ip in hosts.items():
    host_create = zapi.host.create(
        host=host_name,
        interfaces=[{
            "type": 1,
            "main": 1,
            "useip": 1,
            "ip": host_ip,
            "dns": "",
            "port": "10050"
        }],
        groups=[{
            "groupid": group_id
        }],
        templates=[{"templateid": template_id} for template_id in template_ids]
    )

hosts = {
    "vpn":"10.10.10.1",
    "backup_1": "10.10.10.5",
    "backup_2": "10.10.10.6",
    "proxy_mysql": "10.10.10.7"
}
group_name = "Linux servers"  # Nom du groupe d'hôtes
template_names = ["Linux by Zabbix agent"]  # Noms des modèles

# Obtenez l'ID du groupe d'hôtes
group_id = zapi.hostgroup.get(filter={"name": group_name})[0]["groupid"]

# Obtenez les ID des modèles
template_ids = [zapi.template.get(filter={"host": template_name})[0]["templateid"] for template_name in template_names]

for host_name, host_ip in hosts.items():
    host_create = zapi.host.create(
        host=host_name,
        interfaces=[{
            "type": 1,
            "main": 1,
            "useip": 1,
            "ip": host_ip,
            "dns": "",
            "port": "10050"
        }],
        groups=[{
            "groupid": group_id
        }],
        templates=[{"templateid": template_id} for template_id in template_ids]
    )

hosts = {
    "semabox":"10.10.10.8"
}
group_name = "Virtual machines"  # Nom du groupe d'hôtes
template_names = ["Linux by Zabbix agent"]  # Noms des modèles

# Obtenez l'ID du groupe d'hôtes
group_id = zapi.hostgroup.get(filter={"name": group_name})[0]["groupid"]

# Obtenez les ID des modèles
template_ids = [zapi.template.get(filter={"host": template_name})[0]["templateid"] for template_name in template_names]

for host_name, host_ip in hosts.items():
    host_create = zapi.host.create(
        host=host_name,
        interfaces=[{
            "type": 1,
            "main": 1,
            "useip": 1,
            "ip": host_ip,
            "dns": "",
            "port": "10050"
        }],
        groups=[{
            "groupid": group_id
        }],
        templates=[{"templateid": template_id} for template_id in template_ids]
    )
