#!/bin/bash

# Démarrer les services Zabbix Agent et Ssh
service zabbix-agent start
service ssh start

apt install resolvconf -y
wg-quick up wg-mspr