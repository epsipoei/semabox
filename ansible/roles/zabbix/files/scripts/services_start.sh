#!/bin/bash

# Démarrer les services Zabbix Agent, Zabbix Server, Nginx et Ssh
service mysql start
service nginx start
service php8.1-fpm start 
service zabbix-agent start
service zabbix-server start
service ssh start