#!/usr/bin/env python3


"""
Menu-Driven Command-Line Interface Script

This script provides a user-friendly menu-driven command-line interface for executing various tasks. It imports functions from the 'menu' module to create interactive menus and utilizes the 'lib.progress' module for tasks with progress tracking. It also interacts with the operating system using the 'os' module.

Usage:
    Run this script using Python 3 to display the main menu and execute the corresponding actions.

Example:
    $ python3 main.py

Modules:
    - lib.menu: Module containing functions to create interactive menus.
    - lib.progress: Module for executing tasks with progress tracking.
    - os: Module providing operating system interaction functionalities.

Main Menu Options:
    - 0: Execute playbook 'deploy_semabox.yml' with a progress target.
    - 1: Enter the 'main_semabox' submenu to perform actions related to the Semabox.
    - 2: Quit the script.

Submenu 'main_semabox' Options:
    - 0: Install 'vim' via apt-get and clear the terminal.
    - 1: Install 'net-tools' via apt-get and clear the terminal.
    - 2: Install 'tcpdump' via apt-get and clear the terminal.
    - 3: Install 'nmap' via apt-get and clear the terminal.
    - 4: Execute playbook 'deploy_zabbix.yml' with a progress target.

Note:
    - Ensure Python 3 is used for running the script.
    - The 'ansible' playbooks and 'main.py' script need to be present in the specified paths.

"""


import random
import subprocess
from lib import menu
from lib.clear import clear_screen
from lib.progress import execute_with_progress
from lib.progress import show_cursor
import os

current_directory = os.getcwd()
root_path = os.path.dirname(current_directory)

if __name__ == "__main__":
    option_menu = menu.main_menu()

    if option_menu == 0 : # Install Semabox
        option_box = menu.main_choice()
        if option_box == 0 : # Install Semabox into Docker
            show_cursor()
            ip_bdd_cma4 = input('ip bdd cma4: ')
            new_endpoint_vpn = input('ip pub du vpn: ')
            
            entreprise = input('Nom Entreprise: ')
            address_entreprise = input('address_entreprise: ')
            siret = input('siret: ')
            presta = input('presta: ')
            
            name_subnet = input('Nom du sous-reseau: ')
            subnet = input('ip du sous reseau (x.x.x.x/x): ')
            num_sonde = random.randint(1, 9999)
            
            # Obtenir l'ancienne adresse IP de la base de données à partir du fichier secret.py
            old_ip_bdd_cma4 = subprocess.check_output(
                "cat sema_os/env/secret.py | grep 'db_host = ' | awk -F'=' '{print $2}' | sed 's/\"//g'",
                shell=True,
                text=True
            ).strip()
            # Get Last Name Client
            old_entreprise = subprocess.check_output(
                "cat ansible/roles/semabox/files/zabbix_agentd.conf | grep 'Hostname=' | awk -F'=' '{print $2}'",
                shell=True,
                text=True
            ).strip()
            # Get Last Endpoint vpn
            old_endpoint_vpn = subprocess.check_output(
                "cat ansible/roles/semabox/files/wg-mspr.conf | grep 'Endpoint =' | awk -F'=' '{print $2}' | sed 's/:13579//g'",
                shell=True,
                text=True
            ).strip()
            # Update data
            os.system(f'sed -i \'s/db_host = "{old_ip_bdd_cma4}"/db_host = "{ip_bdd_cma4}"/g\' sema_os/env/secret.py')
            os.system(f'sed -i \'s/Hostname="{old_entreprise}"/Hostname="{entreprise}"/g\' ansible/roles/semabox/files/zabbix_agentd.conf')
            os.system(f'sed -i \'s/Endpoint = {old_endpoint_vpn}:13579/Endpoint = {new_endpoint_vpn}:13579/g\' ansible/roles/semabox/files/wg-mspr.conf')
            
            # Exécuter les commandes Docker
            #subprocess.run(["docker", "rm", "-f", "my_semabox"])
            #subprocess.run(["docker", "rmi", "my_semabox"])
            subprocess.run(["docker", "build", "-t", "my_semabox-2", "."])
            subprocess.run(["docker", "network", "create", f"--subnet={subnet}", f"{name_subnet}"])
            subprocess.run(["docker", "run", "-dit", "--name", "my_semabox-2", "--hostname", f"{entreprise}", f"--network={name_subnet}", "my_semabox-2"])
            subprocess.run(["docker", "start", "my_semabox-2"])
            # Écrire les valeurs dans le fichier secret
            subprocess.run(["docker", "exec", "-i", "my_semabox-2", "bash", "-c", f"echo 'Nom Entreprise: {entreprise}' > secret"])
            subprocess.run(["docker", "exec", "-i", "my_semabox-2", "bash", "-c", f"echo 'Adresse Entreprise: {address_entreprise}' >> secret"])
            subprocess.run(["docker", "exec", "-i", "my_semabox-2", "bash", "-c", f"echo 'Siret Entreprise: {siret}' >> secret"])
            subprocess.run(["docker", "exec", "-i", "my_semabox-2", "bash", "-c", f"echo 'Nom Prestataire: {presta}' >> secret"])
            subprocess.run(["docker", "exec", "-i", "my_semabox-2", "bash", "-c", f"echo 'subnet: {subnet}' >> secret"])
            subprocess.run(["docker", "exec", "-i", "my_semabox-2", "bash", "-c", f"echo 'num_sonde: {num_sonde}' >> secret"])

            
            clear_screen()
            subprocess.run(
                ["docker", "exec", "-i", "my_semabox-2", "bash", "-c", "python3 gui_bar_progress.py --path_ansible ./ansible/playbooks/deploy_semabox.yml --total 38"])
            
        if option_box == 1 : # Install Semabox into Localhost
            show_cursor()
            entreprise = input('Nom Entreprise: ')
            address_entreprise = input('address_entreprise: ')
            siret = input('siret: ')
            presta = input('presta: ')
            
            data_to_write = f"Nom Entreprise: {entreprise}\nAdresse Entreprise: {address_entreprise}\nSiret Entreprise: {siret}\nNom Prestataire: {presta}\n"
            
            with open('secret', 'w') as f:
                # Écrire la réponse de l'utilisateur dans le fichier
                f.write(data_to_write)
            
            clear_screen()   
            execute_with_progress("./ansible/playbooks/deploy_semabox.yml",38)
            
        option = menu.return_or_exit()
        if option == 0:
            os.system("python3 ./main.py")
        elif option == 1:
            clear_screen()
            show_cursor()
            raise SystemExit()
        
    elif option_menu == 1: # Install Service
        option_box = menu.main_choice()
        if option_box == 0 : # Install Service into Docker
            option_box= menu.main_semabox()    
            if option_box == 0:
                os.system("bash -c apt-get update && apt-get install -y vim")
            elif option_box == 1:
                os.system("bash -c apt-get update && apt-get install -y net-tools")
            elif option_box == 2:
                os.system("bash -c apt-get update && apt-get install -y tcpdump")
            elif option_box == 3:
                os.system("bash -c apt-get update && apt-get install -y nmap")
            elif option_box == 4:
                clear_screen()
                show_cursor()
                # Get Last Endpoint vpn
                old_endpoint_vpn = subprocess.check_output(
                    "cat zabbix/ansible/roles/zabbix/files/env/wg-mspr.conf | grep 'Endpoint =' | awk -F'=' '{print $2}' | sed 's/:13579//g'",
                    shell=True,
                    text=True
                ).strip()
                new_endpoint_vpn = input('ip pub du vpn: ')
                os.system(f'sed -i \'s/Endpoint = {old_endpoint_vpn}:13579/Endpoint = {new_endpoint_vpn}:13579/g\' zabbix/ansible/roles/zabbix/files/env/wg-mspr.conf')
                name_container = input('nom pour le container: ')
                subnet = input('IP du subnet (x.x.x.x/x): ')
                name_subnet = input('Nom du subnet: ')
                subprocess.run(["docker", "build", "-t", f"{name_container}", "zabbix/"])
                subprocess.run(["docker", "network", "create", f"--subnet={subnet}", f"{name_subnet}"])
                subprocess.run(["docker", "run", "-dit", "--name", f"{name_container}", f"--network={name_subnet}", f"{name_container}"])
                subprocess.run(["docker", "start", f"{name_container}"])
                subprocess.run(
                ["docker", "exec", "-i", f"{name_container}", "bash", "-c", "python3 gui_bar_progress.py --path_ansible ./ansible/playbooks/deploy_zabbix.yml --total 75"])
        elif option_box == 1:
            option_box= menu.main_semabox()    
            if option_box == 0:
                os.system("bash -c apt-get update && apt-get install -y vim")
            elif option_box == 1:
                os.system("bash -c apt-get update && apt-get install -y net-tools")
            elif option_box == 2:
                os.system("bash -c apt-get update && apt-get install -y tcpdump")
            elif option_box == 3:
                os.system("bash -c apt-get update && apt-get install -y nmap")
            elif option_box == 4:
                clear_screen()
                execute_with_progress("./ansible/playbooks/deploy_zabbix.yml",74)
                                  
        option = menu.return_or_exit()
        if option == 0:
            os.system("python3 ./main.py")
        elif option == 1:
            clear_screen()
            show_cursor()
            raise SystemExit()
        
    elif option_menu == 2:
        quit
