FROM my_sema_ubuntu

RUN mkdir semabox

COPY lib /semabox/lib
COPY classes /semabox/classes
COPY main.py /semabox/main.py
COPY ansible /semabox/ansible
COPY sema_os /semabox/sema_os
COPY Makefile /semabox/Makefile

RUN chmod +x /semabox/main.py
RUN mv /semabox/lib/gui_bar_progress.py /semabox/gui_bar_progress.py

# Expose necessary ports
EXPOSE 8080/tcp 8080/udp \
    6033/tcp 6033/udp \
    10050/tcp 10050/udp \
    10051/tcp 10051/udp


ENTRYPOINT ["/bin/bash"]
