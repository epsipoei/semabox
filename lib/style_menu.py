import shutil

def style():
    """
    Generate a stylized title and version string for the application.

    Returns:
        str: Stylized title and version string.
    """
    date = "2023-07-01"
    version = 1.0
    vers = str(version)

    title = "SEMABOX"
    centered_title = title.center(50)
    centered_vers = vers.center(50)

    # Unicode characters for drawing a continuous line
    top = u"\u250C" + u"\u2500" * 50 + u"\u2510"
    middle_title = u"\u2502" + centered_title + u"\u2502"
    middle_vers = u"\u2502" + centered_vers + u"\u2502"
    bottom = u"\u2514" + u"\u2500" * 50 + u"\u2518"

    text = "\n"
    text += top + "\n"
    text += middle_title + "\n"
    text += middle_vers + "\n"
    text += bottom + "\n"

    # Get the width of the console
    terminal_width = shutil.get_terminal_size().columns

    # Divide the width by two, round it, and subtract half the width of the centered text
    padding = int((terminal_width / 2) - (len(text.split("\n")[1]) / 2))

    # Add white padding to the left and right of each text line
    padded_text = "\n".join([" " * padding + line for line in text.split("\n")])

    return padded_text
